---
layout: markdown_page
title: "IR.1.03 - Incident response Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.1.03 - Incident response

## Control Statement

Confirmed incidents are assigned a priority level and managed to resolution. If applicable, GitLab coordinates the incident response with business contingency activities.

## Context

It's important for every issue to be assigned an appropriate severity so that time, effort, and resources can be most effectively allocated. And by having a mechanism to track whether every incident is seen to resolution, every incident is eventually resolved.

## Scope

TBD

## Ownership

TBD
## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.1.03_incident_response.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.1.03_incident_response.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.1.03_incident_response.md).

## Framework Mapping

* ISO
  * A.16.1.1
  * A.16.1.2
  * A.16.1.4
  * A.16.1.5
  * A.16.1.6
  * A.16.1.7
* SOC2 CC
  * CC4.2
  * CC5.1
  * CC5.2
  * CC7.4
  * CC7.5
* PCI
  * 10.6.3
  * 10.8.1
  * 12.10.3
