---
layout: markdown_page
title: "TRN.1.02 - Code of Conduct Training Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TRN.1.02 - Code of Conduct Training

## Control Statement

All GitLabbers complete a code of business conduct training.

## Context

The aim of this control is help ensure that all GitLabbers are aligned on the values of the organization. The purpose of this alignment is to demonstrate to any external auditors that we hold all GitLabbers to this same standard of conduct.

## Scope

This control applies to all GitLabbers and contractors.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.02_code_of_conduct_training.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.02_code_of_conduct_training.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TRN.1.02_code_of_conduct_training.md).

## Framework Mapping

* ISO
  * A.11.2.8
  * A.7.1.2
  * A.7.2.1
  * A.8.1.3
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
* PCI
  * 12.3
  * 12.3.5
