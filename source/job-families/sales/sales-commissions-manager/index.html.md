---
layout: job_family_page
title: "Sales Commissions Manager"
---

The Sales Commissions Manager leads and manages GitLab's field sales commissions programs globally.  This requires collaboration with cross-functional departments and leveraging systems for all related business processes in a fast-paced distributed environment. This requires someone to be extremely well organized, analytical and detailed-oriented, with a solid knowledge of Sales Incentive Compensation best practices and related operational execution to help drive innovative solutions and optimize field effectiveness.  In particular be an expert administrator with Xactly and have managed all aspects of end-to-end commissions processes.  The Sales Commissions Manager supports the vision of Gitlab’s sales leadership team and reports to our Sr. Manager, Sales Strategy.

### Responsibilities

* Own worldwide field sales commissions calculations, reporting, policies and provide first-tier support for all end-user technical or commission inquiries 
* Partner with various functions such as Sales Operations, People Operations and Finance to establish best practices, as well as drive on-going process refinement to improve the efficiency and accuracy of sales commissions methodology, calculations and reporting
* Provides high quality, timely responses to the field sales and sales management and communicates complex logic and analysis in a manner that is consumable 
* Ownership of commissions systems process and infrastructure, along with project manage implementation of any changes
* Participate as a key member of the Sales Incentive team and provide guidance on global design, delivery of deployed plans, governance, controls, metrics and best practices 
* Research compensation issues, disputes, appeals and provide analysis to facilitate resolution.
* Responsible for timely and accurate preparation of monthly accruals, deferrals, reconciliations and provide fluctuation analysis for commissions accounting
* Train Field Sales new hires on compensation plans and system
* Maintain data integrity within Xactly, Salesforce.com and other related systems
* Assist external audit firm on commission related matters for quarterly reviews and annual audits and ad hoc requests
* Ensure compliance with SOX for compensation documents, processes, programs and policies

### Requirements

* BA/BS degree in accounting, finance, economics or other quantitative fields preferred
* 5+ years relevant experience and a solid understanding of sales incentive programs, processes and procedures  
* Advanced analytical and modeling skills with the ability to interpret and analyze data
* Ability to partner, collaborate and influence across functional areas (e.g. Finance, People Operations and Sales) and support multiple business partners
* Excellent problem solving, project management, interpersonal and organizational skills
* Deep SFDC, Xactly expertise and knowledge of typical enterprise SaaS tools 
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values.

### Hiring Process
* Screening call with recruiter 
* Interview with the Hiring Manager
* Interview with 2-3 additional team members
* Final interview with Executive 
